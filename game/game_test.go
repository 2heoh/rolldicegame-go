package game

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestShowBalanceWithRunApp(t *testing.T) {

	game, err := NewGame()

	result, err := game.Start()

	assert.Equal(t, "Welcome Player, your balance is 50 chips", result)
	assert.Nil(t, err)
}

func TestPlayerSetBet(t *testing.T) {

	game, err := NewGame()

	bet := &Bet{
		Number: 1,
		Chips:  50,
	}

	err = game.SetBet(bet)

	assert.Equal(t, 0, game.Player.balance)
	assert.Nil(t, err)
}

func TestShowBets(t *testing.T) {

	game, err := NewGame()

	bet := &Bet{
		Number: 1,
		Chips:  50,
	}
	err = game.SetBet(bet)

	r, err := game.ShowBets()

	assert.Equal(t, "Your current Bet: 50 on number 1", r)
	assert.Nil(t, err)
}
