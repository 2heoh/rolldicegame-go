package game

import "fmt"

type Game struct {
	Player *Player
	Bet    *Bet
}

type Bet struct {
	Number int
	Chips  int
}

func NewGame() (*Game, error) {
	game := &Game{}
	player := &Player{balance: 50}
	game.Player = player
	return game, nil
}

func (g *Game) Start() (string, error) {
	msg := fmt.Sprintf("Welcome Player, your balance is %d chips", g.Player.balance)
	return msg, nil
}

func (g *Game) SetBet(bet *Bet) error {
	g.Bet = bet
	g.Player.balance -= bet.Chips
	return nil
}

func (g *Game) ShowBets() (string, error) {
	msg := fmt.Sprintf("Your current Bet: %d on number %d", g.Bet.Chips, g.Bet.Number)
	return msg, nil
}
