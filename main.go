package main

import (
	"fmt"
	. "gitlab.com/2heoh/rolldicegame-go/game"
)

func main() {
	game, err := NewGame()
	if err == nil {
		result, err := game.Start()
		if err == nil {
			fmt.Println(result)
		}
	}
}
