package ui

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUserInput(t *testing.T) {

	var msg string

	r := Reader{}

	bet, err := r.GetBet(msg)

	assert.Equal(t, &Bet{1, 2}, bet)
	assert.Nil(err)
}
