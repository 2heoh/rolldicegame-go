package ui

type Reader struct {
}

type GamerReader interface {
	Read() (*Bet, error)
	Write(string) error
}

func NewReader() {
	reader := Reader{}
}
